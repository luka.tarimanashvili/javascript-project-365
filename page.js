//import functions where we go in prodcts or categories
import { products } from './api.js';

//import products style and products html
import { productStyleHTML } from './products.js';

//function where we import products html to html file
const catalog = document.getElementsByClassName('catalog__section')[0];

const fullCatalog = async (sort = null) => {
    const list = await products(sort);
    catalog.innerHTML = productStyleHTML(list);
}

fullCatalog();

//function where we sort files by their place in array
const sort = document.getElementById('sort');
sort.addEventListener('change', () => {
    fullCatalog(sort.value);
});


// const srchQuery = document.getElementById('srchQuery');
// const srchButton = document.getElementById('srchButton');

// srchQuery.value;

// fullCatalog().then(allProducts => {
//     for(let product of allProducts) {
//         console.log(product.title);
//     }
// })
// srchButton.addEventListener('click', () => {
//     products(sort).then(result => {
//         const filterProducts = result.filter(product => product.title.includes(search));
//         catalog.innerHTML = productStyleHTML(filterProducts);
//         console.log(filterProducts);
//     });
// })