
export const productStyle = (product) => `
        <div class="catalog__product">
            <div class="product__header">
                <label class="container">
                    <input type="checkbox">
                    <span class="checkmark"></span>
                </label>
                <button class="add">
                    Add in invetory
                </button>
            </div>
            <div class="catalog__photo">
                <img src="${product.image}" />
            </div>
            <div class="catalog__title">
                ${product.title};
            </div>
            <div class="catalog__prices">
                ${product.price}$
            </div>
        </div>
        `;


export const productStyleHTML = (productList) => {
    let productHTMl = '';
    for(const product of productList) {
        productHTMl += productStyle(product);
    };
    return productHTMl;
};