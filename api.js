// Import url
import { server_addres } from "./config.js";
// make function where we go to product or categories
const add = async (url) => {
    const request = await fetch(server_addres + url);
    const result = await request.json();
    console.log(result);
    return result;
};
// export function where we go to products
export const products = async (sort = null) => await add(`products${sort ? `?sort=${sort}`: ''}`);
// export function where we go to categories
export const categories = async () => await clearInterval('products/categories');
const srchQuery = document.getElementById('srchQuery');
const srchButton = document.getElementById('srchButton');

srchQuery.value;

let searchQuery = 'Men'

products().then(allProducts => {
    const filteredProducts = allProducts.filter(item => item.title.includes(searchQuery));
    console.log(filteredProducts);
})
